package critCrafter;

import org.tbot.bot.TBot;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.event.listeners.InventoryListener;
import org.tbot.internal.event.listeners.PaintListener;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.*;
import org.tbot.methods.ge.GrandExchange;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Walking;
import org.tbot.util.Filter;
import org.tbot.wrappers.*;

import java.awt.*;

/**
 * Created by Shaneee on 30/06/2015.
 */
@Manifest(authors = "Crithane", version = 0.1, name = "Crit Crafter")
public class mainHandler extends AbstractScript implements PaintListener{
    boolean firstTime = true;
    public static int cowhide;
    public long cowhideCount = 0, leatherCount;
    public int cowhideLimit = Random.nextInt(449, 500);
    Area cowPen = new Area(new Tile[]  {new Tile(3253,3255,0),new Tile(3254,3255,0),new Tile(3255,3255,0),new Tile(3256,3255,0),new Tile(3257,3255,0),new Tile(3257,3255,0),new Tile(3259,3255,0),new Tile(3260,3255,0),new Tile(3261,3255,0),new Tile(3262,3255,0),new Tile(3263,3255,0),new Tile(3264,3255,0),new Tile(3265,3255,0),new Tile(3265,3255,0),new Tile(3265,3257,0),new Tile(3265,3277,0),new Tile(3265,3296,0),new Tile(3264,3297,0),new Tile(3263,3298,0),new Tile(3262,3298,0),new Tile(3261,3298,0),new Tile(3260,3299,0),new Tile(3257,3299,0),new Tile(3257,3296,0),new Tile(3254,3296,0),new Tile(3254,3298,0),new Tile(3249,3298,0),new Tile(3249,3297,0),new Tile(3246,3297,0),new Tile(3246,3298,0),new Tile(3246,3296,0),new Tile(3243,3296,0),new Tile(3243,3298,0),new Tile(3241,3298,0),new Tile(3240,3297,0),new Tile(3240,3296,0),new Tile(3241,3295,0),new Tile(3241,3294,0),new Tile(3242,3293,0),new Tile(3242,3288,0),new Tile(3241,3287,0),new Tile(3240,3286,0),new Tile(3240,3285,0),new Tile(3242,3283,0),new Tile(3244,3281,0),new Tile(3244,3280,0),new Tile(3245,3279,0),new Tile(3246,3278,0),new Tile(3249,3278,0),new Tile(3248,3279,0),new Tile(3248,3281,0),new Tile(3251,3281,0),new Tile(3251,3279,0),new Tile(3252,3279,0),new Tile(3252,3277,0),new Tile(3251,3276,0),new Tile(3251,3274,0),new Tile(3253,3273,0),new Tile(3253,3268,0),new Tile(3253,3256,0)});
    int runEn = Random.nextInt(24,45);
    private critCrafter.Timer t = new Timer();
    public long totalCowhide = 0;
    long c;
    public int exchanging, cowhidesSold;
    public int leatherToBuy = Random.nextInt(207,230);
    public int leatherNeeed = 207;
    public int needleNeeded = 0;
    public int threadNeeded = 0;
    public int threading = 0;

    @Override
    public boolean onStart() {
        if(firstTime == true) {
            critCrafter.GUI.run();
            firstTime = false;
        }
        return true;
    }

    @Override
    public int loop() {
        c = t.getMintuesElapsed();
        Mouse.setSpeed(Random.nextInt(30, 35));
        if(Walking.getRunEnergy() >= runEn){
            if(!Walking.isRunEnabled()){
                Walking.setRun(true);
                methods.dynamicSleep(Walking.isRunEnabled(), 800,1200);
            }
        }
                if (waitforInput()) {
                    sleep(600, 800);
                } else if (collectCowhide() && !needtoBank() && !isExchanging() && !craftLeather()) {
                    if (cowPen.contains(Players.getLocal().getLocation().toRegionTile())) {
                        GroundItem cowhide = GroundItems.getNearest("Cowhide");
                        if (cowhide != null && cowhide.isOnScreen()) {
                            cowhide.pickUp();
                            methods.dynamicSleep(methods.doingNothing(), 800, 1200);
                        } else if (cowhide != null && !cowhide.isOnScreen()) {
                            methods.walkTo(cowhide.getLocation(), 5);
                            if (cowhide.distance() > 5) {
                                methods.dynamicSleep(cowhide.isOnScreen(), 800, 1200);
                            } else {
                                Camera.turnTo(cowhide);
                                methods.dynamicSleep(cowhide.isOnScreen(), 800, 1200);
                            }
                        } else {
                            sleep(600, 800);
                        }
                        //In cow pen
                    } else {
                        methods.walkTo(cowPen.getCentralTile(), 5);
                        methods.dynamicSleep(cowPen.contains(Players.getLocal().getLocation().toRegionTile()), 800, 1200);
                    }
                } else if (buyLeather() && !collectCowhide() || isExchanging() && !leatherBanking() && !collectCowhide()) {
                    if (!Bank.isOpen() && Inventory.getCount("Cowhide") != totalCowhide && !isExchanging()) {
                        Bank.openNearestBank();
                        methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                    } else if (Bank.isOpen() && Inventory.getCount("Cowhide") != totalCowhide && !isExchanging()) {
                        totalCowhide = Bank.getCount("Cowhide") + Inventory.getCount("Cowhide");
                        if (timeToSell()) {
                            if (Inventory.getCount("Cowhide") != totalCowhide) {
                                Bank.depositAll();
                                methods.dynamicSleep(Inventory.getEmptySlots() == 28, 1000, 1200);
                                if (!Bank.isNoted()) {
                                    Bank.setNoted(true);
                                    methods.dynamicSleep(Bank.isNoted(), 1000, 1200);
                                }
                                Bank.withdrawAll("Cowhide");
                                methods.dynamicSleep(Inventory.getCount("Cowhide") == totalCowhide, 1000, 1200);
                            } else {
                                Bank.close();
                                methods.dynamicSleep(!Bank.isOpen(), 1000, 1200);
                            }

                        }
                    } else if (!Bank.isOpen() && Inventory.getCount("Cowhide") == totalCowhide || isExchanging() && !leatherBanking()) {
                        exchanging = 1;
                        if (GrandExchange.isOpen()) {
                            if (GrandExchange.getOffer("Cowhide") != null && GrandExchange.getOffer("Cowhide").isCompleted()) {
                                cowhidesSold = 1;
                                GrandExchange.collectAll();
                                return Random.nextInt(600, 800);
                            } else if (Inventory.contains("Cowhide")) {
                                GrandExchange.placeSellOfferPercentage(Inventory.getFirst("Cowhide"), Inventory.getCount("Cowhide"), 85);
                                return Random.nextInt(600, 800);
                            } else if (cowHidesSold() && !craftLeather()) {
                                if (GrandExchange.getOffer("Leather") != null && GrandExchange.getOffer("Leather").isCompleted()) {
                                    cowhidesSold = 1;
                                    GrandExchange.collectAll();
                                    return Random.nextInt(600, 800);
                                } else if (!Inventory.contains("Leather") && GrandExchange.getOffer("Leather") == null) {
                                    GrandExchange.placeBuyOffer(1741, leatherToBuy, 210);
                                    return Random.nextInt(600, 800);
                                } else if (Inventory.getCount("Leather") >= leatherNeeed) {
                                    leatherCount = Inventory.getCount("Leather");
                                    exchanging = 0;
                                }
                            }
                        } else {
                            GrandExchange.openMainScreen();
                            methods.dynamicSleep(GrandExchange.isMainScreenOpen(), 1000, 1200);
                        }
                    } else if (Bank.isOpen() && Inventory.getCount("Cowhide") == totalCowhide && !isExchanging()) {
                        totalCowhide = Bank.getCount("Cowhide") + Inventory.getCount("Cowhide");
                        Bank.close();
                        methods.dynamicSleep(!Bank.isOpen(), 1000, 1200);
                    }
                } else if (craftLeather() && !needtoBank() && !isExchanging() && !leatherBanking() && !collectCowhide()) {
                    WidgetChild cont = Widgets.getWidgetByText("Click here to continue");
                    if (cont != null) {
                        methods.handleContinue();
                        threading = 0;
                    }
                    if (GrandExchange.isOpen()) {
                        GrandExchange.close();
                        sleep(600, 800);
                    }
                    if (Bank.isOpen()) {
                        Bank.close();
                        sleep(600, 800);
                    }
                    if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 7) {
                        WidgetChild gloves = Widgets.getWidget(154, 108);
                        if (!gloves.isOnScreen() && threading == 0) {
                            Inventory.useItemOn(1733, 1741);
                            methods.dynamicSleep(threading == 1, 1000, 1200);
                        } else if (gloves.isOnScreen()) {
                            threading = 1;
                            Widgets.getWidget(154, 108).interact("Make All pairs of");
                            sleep(1000, 1200);
                        } else if (!gloves.isOnScreen() && threading == 1) {
                            if (Inventory.contains("Leather")) {
                                sleep(1000, 1200);
                            }
                        }
                        //Gloves
                    } else if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 9 && Skills.getCurrentLevel(Skills.Skill.Crafting) >= 7) {
                        WidgetChild boots = Widgets.getWidget(154, 109);
                        if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 9) {
                            if (!boots.isOnScreen() && threading == 0) {
                                Inventory.useItemOn(1733, 1741);
                                methods.dynamicSleep(threading == 1, 1000, 1200);
                            } else if (boots.isOnScreen()) {
                                threading = 1;
                                boots.interact("Make All pairs of");
                                sleep(1000, 1200);
                            } else if (!boots.isOnScreen() && threading == 1) {
                                if (Inventory.contains("Leather")) {
                                    sleep(1000, 1200);
                                }
                            }
                        }
                        //Boots
                    } else if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 11 && Skills.getCurrentLevel(Skills.Skill.Crafting) >= 9) {
                        WidgetChild cowl = Widgets.getWidget(154, 113);
                        if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 11) {
                            if (!cowl.isOnScreen() && threading == 0) {
                                Inventory.useItemOn(1733, 1741);
                                methods.dynamicSleep(threading == 1, 1000, 1200);
                            } else if (cowl.isOnScreen()) {
                                threading = 1;
                                cowl.interact("Make All");
                                sleep(1000, 1200);
                            } else if (!cowl.isOnScreen() && threading == 1) {
                                if (Inventory.contains("Leather")) {
                                    sleep(1000, 1200);
                                }
                            }
                        }
                        //Cowl
                    } else if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 14 && Skills.getCurrentLevel(Skills.Skill.Crafting) >= 11) {
                        WidgetChild vamb = Widgets.getWidget(154, 110);
                        if (Skills.getCurrentLevel(Skills.Skill.Crafting) < 14) {
                            if (!vamb.isOnScreen() && threading == 0) {
                                Inventory.useItemOn(1733, 1741);
                                methods.dynamicSleep(threading == 1, 1000, 1200);
                            } else if (vamb.isOnScreen()) {
                                threading = 1;
                                vamb.interact("Make All pairs of");
                                sleep(1000, 1200);
                            } else if (!vamb.isOnScreen() && threading == 1) {
                                if (Inventory.contains("Leather")) {
                                    sleep(1000, 1200);
                                }
                            }
                        }
                        //Vambraces
                    } else if (Skills.getCurrentLevel(Skills.Skill.Crafting) >= 14 && Skills.getCurrentLevel(Skills.Skill.Crafting) < 20) {
                        WidgetChild body = Widgets.getWidget(154, 114);
                        if (Skills.getCurrentLevel(Skills.Skill.Crafting) >= 14) {
                            if (!body.isOnScreen() && threading == 0) {
                                Inventory.useItemOn(1733, 1741);
                                methods.dynamicSleep(threading == 1, 1000, 1200);
                            } else if (body.isOnScreen()) {
                                threading = 1;
                                body.interact("Make All");
                                sleep(1000, 1200);
                            } else if (!body.isOnScreen() && threading == 1) {
                                if (Inventory.contains("Leather")) {
                                    sleep(1000, 1200);
                                }
                            }
                        }
                        //Bodies
                    } else if (Skills.getCurrentLevel(Skills.Skill.Crafting) >= 20) {
                        WidgetChild body = Widgets.getWidget(154, 114);
                        if (Skills.getCurrentLevel(Skills.Skill.Crafting) >= 14) {
                            if (!body.isOnScreen() && threading == 0) {
                                Inventory.useItemOn(1733, 1741);
                                methods.dynamicSleep(threading == 1, 1000, 1200);
                            } else if (body.isOnScreen()) {
                                threading = 1;
                                body.interact("Make All");
                                sleep(1000, 1200);
                            } else if (!body.isOnScreen() && threading == 1) {
                                if (Inventory.contains("Leather")) {
                                    sleep(1000, 1200);
                                }
                            }
                        }
                    }

                } else if (needtoBank()) {
                    if (GrandExchange.isOpen()) {
                        GrandExchange.close();
                        sleep(600, 800);
                    }
                    if (collectCowhide() && !isExchanging()) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 600, 1000);
                        } else {
                            cowhideCount = cowhideCount + Inventory.getCount("Cowhide");
                            totalCowhide = Bank.getCount("Cowhide") + Inventory.getCount("Cowhide");
                            Bank.depositAll();
                            methods.dynamicSleep(Inventory.getEmptySlots() == 28, 600, 1000);
                        }
                    }
                } else if (checkLeather() && !craftLeather() && !leatherBanking() && !collectCowhide()) {
                    LogHandler.log("7");
                    if (GrandExchange.isOpen()) {
                        GrandExchange.close();
                        sleep(600, 800);
                    }
                    Inventory.getCount("Leather");
                    if (!Bank.isOpen()) {
                        Bank.openNearestBank();
                        methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                    } else {
                        leatherCount = Inventory.getCount("Leather") + Bank.getCount("Leather");
                    }
                } else if (leatherBanking() && !buyNeedle() && !buyThread() && !collectCowhide()) {
                    threading = 0;
                    if (GrandExchange.isOpen()) {
                        GrandExchange.close();
                        sleep(600, 800);
                    }
                    if (!Inventory.contains("Needle")) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                        } else {
                            if (Bank.isNoted()) {
                                Bank.setNoted(false);
                                methods.dynamicSleep(!Bank.isNoted(), 1000, 1200);
                            }
                            if (!Inventory.contains("Needle") && !Bank.contains("Needle")) {
                                if (Inventory.getCount("Coins") > 350 && Inventory.getEmptySlots() > 0) {
                                    needleNeeded = 1;
                                } else if (Inventory.getCount("Coins") < 350 && Inventory.getEmptySlots() > 0) {
                                    Bank.withdrawAll("Coins");
                                    methods.dynamicSleep(Inventory.getCount("Coins") > 350, 1000, 1200);
                                } else if (Inventory.getCount("Coins") < 350 && Inventory.getEmptySlots() < 0) {
                                    Bank.depositAll();
                                    methods.dynamicSleep(Inventory.getCount("Coins") > 350, 1000, 1200);
                                    Bank.withdrawAll("Coins");
                                }
                            } else if (Bank.contains("Needle")) {
                                needleNeeded = 0;
                                Bank.withdrawAll("Needle");
                                methods.dynamicSleep(Inventory.contains("Needle"), 1000, 1200);
                            }
                        }
                    } else if (!Inventory.contains("Thread")) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                        } else {
                            if (Bank.isNoted()) {
                                Bank.setNoted(false);
                                methods.dynamicSleep(!Bank.isNoted(), 1000, 1200);
                            }
                            if (!Inventory.contains("Thread") && !Bank.contains("Thread")) {
                                if (Inventory.getCount("Coins") >= 2000 && Inventory.getEmptySlots() > 0) {
                                    threadNeeded = 1;
                                } else if (Inventory.getCount("Coins") < 2000 && Inventory.getEmptySlots() > 0) {
                                    Bank.withdrawAll("Coins");
                                    methods.dynamicSleep(Inventory.getCount("Coins") > 2000, 1000, 1200);
                                } else if (Inventory.getCount("Coins") < 2000 && Inventory.getEmptySlots() < 0) {
                                    Bank.depositAll();
                                    methods.dynamicSleep(Inventory.getCount("Coins") > 2000, 1000, 1200);
                                    Bank.withdrawAll("Coins");
                                }
                            } else {
                                threadNeeded = 0;
                                Bank.withdrawAll("Thread");
                                methods.dynamicSleep(Inventory.contains("Thread"), 1000, 1200);
                            }
                        }
                    } else if (!Inventory.contains(1741)) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                        } else {
                            if (Bank.isNoted()) {
                                Bank.setNoted(false);
                                methods.dynamicSleep(!Bank.isNoted(), 1000, 1200);
                            }
                            if (Inventory.getEmptySlots() < 26 && Inventory.getCount("Leather") < 26) {
                                if (depositItems != null && Inventory.contains(depositItems[0].toString())){
                                    Bank.depositAll(depositItems[0].toString());
                                }
                                methods.dynamicSleep(Inventory.getEmptySlots() == 26, 1000, 1200);
                            } else if (Inventory.getEmptySlots() == 26 && Inventory.getCount("Leather") < 26) {
                                Bank.withdrawAll("Leather");
                                methods.dynamicSleep(Inventory.contains("Leather"), 1000, 1200);
                            }
                        }
                    } else if (Inventory.contains(1742)) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                        } else {
                            if (Bank.isNoted()) {
                                Bank.setNoted(false);
                                methods.dynamicSleep(!Bank.isNoted(), 1000, 1200);
                            }
                            Bank.depositAll(1742);
                            methods.dynamicSleep(!Inventory.contains(1742), 1000, 1200);
                        }
                    } else if (Inventory.contains("Coins")) {
                        if (!Bank.isOpen()) {
                            Bank.openNearestBank();
                            methods.dynamicSleep(Bank.isOpen(), 1000, 1200);
                        } else {
                            if (Bank.isNoted()) {
                                Bank.setNoted(false);
                                methods.dynamicSleep(!Bank.isNoted(), 1000, 1200);
                            }
                            Bank.depositAll("Coins");
                            methods.dynamicSleep(!Inventory.contains("Coins"), 1000, 1200);
                        }
                    }
                } else if (buyNeedle()) {
                    if (Inventory.contains("Needle")) {
                        if (GrandExchange.isOpen()) {
                            GrandExchange.close();
                            sleep(600, 800);
                        }
                        needleNeeded = 0;
                    } else {
                        if (!GrandExchange.isOpen()) {
                            GrandExchange.openMainScreen();
                            methods.dynamicSleep(GrandExchange.isOpen(), 1000, 1200);
                        } else {
                            if (GrandExchange.getOffer("Needle") != null && GrandExchange.getOffer("Needle").isCompleted()) {
                                GrandExchange.collectAll();
                                return Random.nextInt(600, 800);
                            } else if (!Inventory.contains("Needle") && GrandExchange.getOffer("Needle") == null) {
                                GrandExchange.placeBuyOffer(1733, 1, 350);
                                return Random.nextInt(600, 800);
                            }
                        }
                    }
                } else if (buyThread() && !buyNeedle()) {
                    if (Inventory.contains("Thread")) {
                        if (GrandExchange.isOpen()) {
                            GrandExchange.close();
                            sleep(600, 800);
                        }
                        threadNeeded = 0;
                    } else {
                        if (!GrandExchange.isOpen()) {
                            GrandExchange.openMainScreen();
                            methods.dynamicSleep(GrandExchange.isOpen(), 1000, 1200);
                        } else {
                            if (GrandExchange.getOffer("Thread") != null && GrandExchange.getOffer("Thread").isCompleted()) {
                                GrandExchange.collectAll();
                                return Random.nextInt(600, 800);
                            } else if (!Inventory.contains("Thread") && GrandExchange.getOffer("Thread") == null) {
                                GrandExchange.placeBuyOffer(1734, 100, 20);
                                return Random.nextInt(600, 800);
                            }
                        }
                    }
                }
        return 120;
    }

    public boolean collectCowhide(){
        return totalCowhide < cowhideLimit && cowhide == 1;
    }
    public boolean buyLeather(){
        return totalCowhide >= cowhideLimit && cowhide == 1 || cowhide == 2 && Inventory.getCount("Coin") > 50000;
    }

    public boolean timeToSell(){
        return totalCowhide >= cowhideLimit;
    }

    public boolean needtoBank(){
        return collectCowhide() && Inventory.getEmptySlots() == 0;
    }

    public boolean leatherBanking(){
        return Inventory.contains("Coins")  || Inventory.contains(1742) || !Inventory.contains("Needle") || !Inventory.contains(1741) || !Inventory.contains("Thread");
    }

    public boolean waitforInput(){
        return cowhide != 1 && cowhide != 2;
    }

    public boolean craftLeather(){
        return leatherCount > 0;
    }

    public boolean checkLeather(){
        return cowhide == 2;
    }

    public boolean isExchanging(){
        return exchanging == 1;
    }

    public boolean cowHidesSold(){
        return cowhidesSold == 1;
    }

    public boolean buyNeedle(){
        return needleNeeded == 1;
    }

    public boolean buyThread(){
        return threadNeeded == 1;
    }
    public boolean makeXOnScreen(){
        WidgetChild makex = Widgets.getWidgetByText("Enter amount:");
        if(makex != null){
            return makex.isOnScreen();
        }return false;
    }

    Item[] depositItems = Inventory.getItems(new Filter<Item>() {

        @Override
        public boolean accept(Item item) {
            return item != null && !item.getName().toLowerCase().equals("Needle")
                    && !item.getName().toLowerCase().equals("Thread");
        }
    });


    private final Color color1 = new Color(255, 51, 0);
    private final Color color2 = new Color(0, 0, 0);
    private final Color color3 = new Color(51, 255, 255);

    private final BasicStroke stroke1 = new BasicStroke(1);

    private final Font font1 = new Font("Calibri", 1, 16);

    public void onRepaint(Graphics g1) {
        Graphics2D g = (Graphics2D)g1;
        g.setColor(color1);
        g.fillRect(2, 3, 516, 36);
        g.setColor(color2);
        g.setStroke(stroke1);
        g.drawRect(2, 3, 516, 36);
        g.setFont(font1);
        g.drawString("Cowhides Collected: " + cowhideCount, 6, 27);
        g.setColor(color3);
        g.drawString("Time Running: " + t.getFormattedTime() , 5, 56);
    }
}
