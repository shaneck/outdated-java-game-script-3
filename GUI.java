package critCrafter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Shane on 22/06/2015.
 */


public class GUI extends JFrame {


    public static void run(){
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }

        JFrame f = new JFrame();
        JButton Cowhide = new JButton();
        JButton NoCowhide = new JButton();
        JPanel p = new JPanel();


        f.setResizable(false);
        f.setTitle("SCRIPT");
        f.setSize(280, 180);
        f.setLocation(100, 100);
        GridLayout Layout = new GridLayout(2, 2, 10, 10);
        p.setLayout(Layout);
        p.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 10));


        Cowhide.setText("Cowhide");
        NoCowhide.setText("No Cowhide");

        p.add(Cowhide);
        p.add(NoCowhide);
        f.add(p);
        f.add(p);
        f.pack();
        f.setVisible(true);

        Cowhide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cowhide.setText("Started");
                mainHandler.cowhide = 1;
                Create();
                f.dispose();
            }
        });
        NoCowhide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NoCowhide.setText("Started");
                mainHandler.cowhide = 2;
                Create();
                f.dispose();
            }
        });
        if(mainHandler.cowhide > 0){
            f.dispose();
        }
    }

    public static void Create(){
    // send back to main handler
    }



}