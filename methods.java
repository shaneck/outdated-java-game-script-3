package critCrafter;

import org.tbot.methods.*;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Path;
import org.tbot.methods.walking.Walking;
import org.tbot.util.Condition;
import org.tbot.wrappers.*;
import tutorialIsland.Variables;

import java.awt.event.KeyEvent;
import java.util.TreeMap;

import static org.tbot.methods.Time.sleep;


/**
 * Created by Shane on 10/06/2015.
 */
public class methods {
    public static void talkTo(String npcName) {
        WidgetChild widget = Widgets.getWidgetByText("Click here to continue");
        WidgetChild widget2 = Widgets.getWidgetByText("Click to continue");
        if (!NPCChat.isChatOpen()) {
            if (Npcs.getNearest(npcName) != null && Npcs.getNearest(npcName).isOnScreen()) {
                NPC npcTalkTo = Npcs.getNearest(npcName);
                if(npcTalkTo != null) {
                    npcTalkTo.interact("Talk-to");
                    sleep(800, 900);
                }
            }else {
                NPC npc1 = Npcs.getNearest(npcName);
                if (npc1 != null) {
                    Path walking = Walking.findLocalPath(npc1);
                    if (walking != null) {
                        walking.traverse();
                        sleep(800, 900);
                    }
                }
            }

        } else{
            NPCChat.clickContinue();
            dynamicSleep(!NPCChat.canContinue(), 600, 800);
        }

    }

    public boolean inArea(Area area){
        return area.contains(Players.getLocal().getLocation().toRegionTile());
    }

    public static void continueMethod() {
        if (tutorialIsland.Variables.CONTINUE.isOnScreen() || tutorialIsland.Variables.CONTINUE2.isOnScreen() || tutorialIsland.Variables.CONTINUE3.isOnScreen() || tutorialIsland.Variables.CONTINUE4.isOnScreen() ){
            Widgets.clickContinue();
            sleep(100, 200);
        }
    }
    public static void cookFood(){
        GameObject fires = GameObjects.getNearest(26185);
        if(fires != null){
            if(Widgets.isOpen(Widgets.TAB_INVENTORY)) {
                   if(Inventory.useItemOn(2514, fires)){
                    sleep(200, 300);
                    Time.sleepUntil(new Condition() {
                                        @Override
                                        public boolean check() {
                                            return Inventory.contains(315) || Inventory.contains(7954);
                                        }
                                    },
                            Random.nextInt(1000, 1300)
                    );
                }
            }else{
                tutorialIsland.Variables.INVENTORY.interact("Inventory");
                Time.sleepUntil(new Condition() {
                                    @Override
                                    public boolean check() {
                                        return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                    }
                                },
                        Random.nextInt(1000, 1300)
                );
            }
        }else{
            GameObject tree3 = GameObjects.getNearest(9730);
            tree3.interact("Chop");
            Time.sleepUntil(new Condition() {
                                @Override
                                public boolean check() {
                                    return Inventory.containsOneOf(2511);
                                }
                            },
                    Random.nextInt(1000, 1300)
            );
            if(!Widgets.isOpen(Widgets.TAB_INVENTORY)){
                tutorialIsland.Variables.INVENTORY.interact("Inventory");}
            Time.sleepUntil(new Condition() {
                                @Override
                                public boolean check() {
                                    return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                }
                            },
                    Random.nextInt(1000, 1300)
            );
            Inventory.useItemOn(2511, 590);
            Time.sleepUntil(new Condition() {
                                @Override
                                public boolean check() {
                                    return !Inventory.contains(2511);
                                }
                            },
                    Random.nextInt(1000, 1300)
            );
        }
    }

    public static void interactObject(String objectName, String interactOpt){
        sleep(200,300);
        if(GameObjects.getNearest(objectName) != null ) {
            if (GameObjects.getNearest(objectName).isOnScreen() && GameObjects.getNearest(objectName) != null) {
                GameObject objectToInteract = GameObjects.getNearest(objectName);
                if (Players.getLocal().getAnimation() == -1 && GameObjects.getNearest(objectName) != null) {
                    objectToInteract.interact(interactOpt);
                }
                sleep(200, 300);
            } else {
                final Tile objectTile = GameObjects.getNearest(objectName).getLocation();
                Path objectPath = Walking.findLocalPath(objectTile);
                if(objectPath != null) {
                    objectPath.traverse();
                    Time.sleepUntil(new Condition() {
                                        @Override
                                        public boolean check() {
                                            return objectTile.distance() < 3;
                                        }
                                    },
                            Random.nextInt(1000, 1300)
                    );
                }
            }
        }
    }

    public static boolean interactMobject(String object, String interact) {
        GameObject mobject = GameObjects.getNearest(object);
        if (mobject != null) {
            if (mobject.isOnScreen()) {
                if (mobject.interact(interact)) return true;
            }
        }
        return false;
    }

    /* now you can do like if (interactMobject(x,x)){ sleepUntil....... */

    public static void interactObjectID(int objectID, String interactOpt){
        sleep(200,300);
        GameObject object = GameObjects.getNearest(objectID);
        if(object != null && object.isOnScreen()) {
            GameObject objectToInteract = GameObjects.getNearest(objectID);
            objectToInteract.interact(interactOpt);
            sleep(600, 800);
        }else {
            GameObject object1 = GameObjects.getNearest(objectID);
            if (object1 !=null) {
                final Tile objectTile = GameObjects.getNearest(objectID).getLocation();
            Path objectPath = null;
            if (objectTile != null) {
                objectPath = Walking.findLocalPath(objectTile);
            }
            if (objectPath != null) {
                objectPath.traverse();
                Time.sleepUntil(new Condition() {
                                    @Override
                                    public boolean check() {
                                        return objectTile.distance() < 3;
                                    }
                                },
                        Random.nextInt(1000, 1300)
                );
            }
            }
        }
    }

    public static void fishing() {
        if(methods.doingNothing()) {
            methods.interact("Fishing spot", "Net");
        }
        Time.sleepUntil(new Condition() {
                            @Override
                            public boolean check() {
                                return Inventory.containsOneOf("Raw shrimps");
                            }
                        },
                Random.nextInt(1000, 1300)
        );
        sleep(200, 300);
    }

    public static boolean useItemOn(String item,String object){
        if (Inventory.getFirst(item) != null && GameObjects.getNearest(object) != null){
            if(Mouse.click(Inventory.getFirst(item).getRandomPoint(), true)){
                return Mouse.click(GameObjects.getNearest(object).getModel().getRandomPoint(), true);
            }
        }return false;
    }

    public static boolean doingNothing(){
        return Players.getLocal().getAnimation() == -1 && !Players.getLocal().isMoving();
    }

    public static void removeRoof(){
        if(tutorialIsland.Variables.ADVANCED != null && tutorialIsland.Variables.ADVANCED.isOnScreen() && !tutorialIsland.Variables.ROOF.isOnScreen()){
            tutorialIsland.Variables.ADVANCED.interact("View Advanced Options");
            sleep(600,800);
        }else if (!tutorialIsland.Variables.ROOF.isOnScreen()){
            tutorialIsland.Variables.TOOLS.interact("Options");
            sleep(600,800);
        }else if(tutorialIsland.Variables.ROOF.isOnScreen()){
            Variables.ROOF.interact("Roof-removal");
            sleep(600,800);

        }
    }

    public static void walkTo(Tile tile, int distance){
        if(tile != null && tile.distance() > distance){
            Path tilepath = Walking.findPath(tile);
            if(tilepath !=null){
                tilepath.traverse();
                dynamicSleep(tile.distance() < distance, 600,800);
            }
        }
    }
    public static void walkTo2(Tile tile, int distance){
        if(tile != null && tile.distance() > distance){
            Path tilepath = Walking.findLocalPath(tile);
            if(tilepath !=null){
                tilepath.traverse();
                dynamicSleep(tile.distance() < distance, 600,800);
            }
        }
    }

    public static void dynamicSleep(Boolean bool, int int1, int int2){
        Time.sleepUntil(new Condition() {
                            @Override
                            public boolean check() {
                                return bool;
                            }
                        },
                Random.nextInt(int1, int2)
        );
    }

    public static NPC getNearestAttackableNPC(final String... name) {
        NPC[] npcs = Npcs.getLoaded(npc -> {
            if (name.length > 0) {
                for (String n : name) {
                    if (npc.getName().equals(n) && npc.getAnimation() == -1 && npc.getInteractingEntity() == null && !npc.isHealthBarVisible()) {
                        return true;
                    }
                }
            } else return npc.getAnimation() == -1 && npc.getInteractingEntity() == null && !npc.isHealthBarVisible();

            return false;
        });
        TreeMap<Integer, NPC> npcMap = new TreeMap<>();
        for(NPC n : npcs) {
            npcMap.putIfAbsent(Walking.getRealDistanceTo(n.getLocation()), n);
        }
        return npcMap.get(0);

    }

public static void handleContinue(){
    WidgetChild widget = Widgets.getWidgetByText("Click here to continue");
    if (widget != null && widget.isOnScreen()){
        org.tbot.methods.input.keyboard.Keyboard.pressKey((char) KeyEvent.VK_SPACE);
    }else{
        org.tbot.methods.input.keyboard.Keyboard.releaseKey((char) KeyEvent.VK_SPACE);
    }
}
    public static void handleContinue2() {
        WidgetChild widget = Widgets.getWidgetByText("Click to continue");
        if (widget != null && widget.isOnScreen()) {
            Widgets.getWidgetByText("Click to continue");
            widget.click();
        }  // i need to buy a new mouse by the time we're done with this script l0l
    }

    public static void interact(String npcName, String action){
        if(Npcs.getNearest(npcName) != null && Npcs.getNearest(npcName
        ).isOnScreen()) {
            NPC npcToInteractWith = Npcs.getNearest(npcName);
            if(Npcs.getNearest(npcName) != null) {
                npcToInteractWith.interact(action);
                sleep(200, 300);
            }
        }else{
            final Tile objectTile = Npcs.getNearest(npcName).getLocation();
            Path objectPath = Walking.findLocalPath(objectTile);
            if(Walking.findLocalPath(objectTile) != null) {
                objectPath.traverse();
                Time.sleepUntil(new Condition() {
                                    @Override
                                    public boolean check() {
                                        return objectTile.distance() < 3;
                                    }
                                },
                        Random.nextInt(1000, 1300)
                );
            }
        }
    }
}
